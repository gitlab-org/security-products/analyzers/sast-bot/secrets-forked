package config

import (
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

// DefaultPathGitleaksConfig is the default path for the Gitleaks configuration file.
const DefaultPathGitleaksConfig = "/gitleaks.toml"

// GitleaksPassthroughTarget is the target filename for Gitleaks configuration in passthrough scenarios.
const GitleaksPassthroughTarget = "gitleaks.toml"

// Path determines the appropriate path for the Gitleaks configuration file based on the provided ruleset configuration.
// It handles various passthrough scenarios and falls back to the default path if no specific configuration is provided.
func Path(rulesetConfig *ruleset.Config) (string, error) {
	if rulesetConfig != nil && len(rulesetConfig.Passthrough) != 0 {
		rulesetPath, err := ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
		if err != nil {
			return "", err
		}

		for _, passthrough := range rulesetConfig.Passthrough {
			if passthrough.Type == ruleset.PassthroughGit {
				// Every other type of passthrough is properly processed by `ruleset` module, but we need
				// to make sure this works for `git` passthrough too because the path returned from `ruleset` only
				// points to the `/tmp/ruledir*` directory, and not to the full path including the configuration file.
				return filepath.Join(rulesetPath, GitleaksPassthroughTarget), nil
			}
		}

		// Return the path determined by `ruleset.ProcessPassthroughs` method (see exception above for `git` passthroughs).
		return rulesetPath, nil
	}

	// If no ruleset config exist or passthroughs defined, use default path (i.e. /gitleaks.toml)
	return DefaultPathGitleaksConfig, nil
}
