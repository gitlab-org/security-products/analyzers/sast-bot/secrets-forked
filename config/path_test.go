package config_test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/config"
)

func TestConfigPath(t *testing.T) {
	tmpdir, err := os.MkdirTemp("/tmp", "test")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer os.Remove(tmpdir)

	tests := []struct {
		name string
		in   *ruleset.Config
		want string
	}{
		{
			name: "No custom ruleset configuration or passthroughs defined",
			in:   nil,
			want: config.DefaultPathGitleaksConfig,
		},
		{
			name: "Passthrough of type file",
			in: &ruleset.Config{
				Timeout: 60,
				Path:    tmpdir,
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughFile,
						Target: config.DefaultPathGitleaksConfig,
						Value:  config.DefaultPathGitleaksConfig,
					},
				},
			},
			want: config.DefaultPathGitleaksConfig,
		},
		{
			name: "Passthrough of type url",
			in: &ruleset.Config{
				Timeout: 60,
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughFileURL,
						Target: filepath.Join(tmpdir, config.DefaultPathGitleaksConfig),
						Value:  "https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/raw/main/config/gitleaks.toml",
					},
				},
			},
			want: filepath.Join(tmpdir, config.DefaultPathGitleaksConfig),
		},
		{
			name: "Passthrough of type git",
			in: &ruleset.Config{
				Timeout: 60,
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughGit,
						Target: tmpdir,
						Ref:    "main",
						Subdir: "config",
						Value:  "https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test",
					},
				},
			},
			want: filepath.Join(tmpdir, config.DefaultPathGitleaksConfig),
		},
		{
			name: "Passthrough of type raw",
			in: &ruleset.Config{
				TargetDir: tmpdir,
				Timeout:   60,
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughRaw,
						Target: filepath.Join(tmpdir, config.DefaultPathGitleaksConfig),
						Value:  "raw gitleaks configuration",
					},
				},
			},
			want: filepath.Join(tmpdir, config.DefaultPathGitleaksConfig),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path, err := config.Path(tt.in)

			if err != nil {
				t.Errorf("Expected a nil err, but got: %v", err)
			}

			assert.Equal(t, tt.want, path)
		})
	}
}
