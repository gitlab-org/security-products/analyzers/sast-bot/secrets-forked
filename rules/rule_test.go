package rules_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
)

func TestRule_DescriptionAndRemediation(t *testing.T) {
	testCases := []struct {
		name        string
		rule        rules.Rule
		expectedOut string
	}{
		{
			name: "Basic case",
			rule: rules.Rule{
				ID:          "test_rule",
				Title:       "Test Title",
				Description: "Test Description",
				Remediation: "Test Remediation",
			},
			expectedOut: "Test Description\n\nTest Remediation",
		},
		{
			name: "Empty description",
			rule: rules.Rule{
				ID:          "empty_desc",
				Title:       "Empty Description",
				Description: "",
				Remediation: "Some Remediation",
			},
			expectedOut: "Some Remediation",
		},
		{
			name: "Empty remediation",
			rule: rules.Rule{
				ID:          "empty_remed",
				Title:       "Empty Remediation",
				Description: "Some Description",
				Remediation: "",
			},
			expectedOut: "Some Description",
		},
		{
			name: "Both empty",
			rule: rules.Rule{
				ID:          "both_empty",
				Title:       "Both Empty",
				Description: "",
				Remediation: "",
			},
			expectedOut: "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result := tc.rule.DescriptionAndRemediation()
			require.Equal(t, tc.expectedOut, result)
		})
	}
}
