package rules_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/mock"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
)

func TestFind(t *testing.T) {

	testCases := []struct {
		name             string
		searchRuleID     string
		rules            rules.Rules
		expectedRuleID   string
		expectedTitle    string
		expectedErrorStr string
	}{
		{
			name:         "Find rule that exists",
			searchRuleID: "testing_2",
			rules: mock.MakeRules(
				[]rules.Rule{
					mock.MakeRule("testing_1"),
					mock.MakeRule("testing_2"),
					mock.MakeRule("testing_3"),
				},
			),
			expectedRuleID: "testing_2",
		},
		{
			name:         "Find duplicate rule ID",
			searchRuleID: "testing_1",
			rules: mock.MakeRules(
				[]rules.Rule{
					{
						ID:    "testing_1",
						Title: "Override Rule Title",
					},
					mock.MakeRule("testing_2"),
					{
						ID:    "testing_1",
						Title: "Extended Rule Title",
					},
				},
			),
			expectedRuleID: "testing_1",
			expectedTitle:  "Override Rule Title",
		},
		{
			name:             "Find rule that does not exist",
			searchRuleID:     "does_not_exist",
			rules:            mock.MakeRules([]rules.Rule{}),
			expectedRuleID:   "",
			expectedErrorStr: "rule does_not_exist not found",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {

			lookup := rules.NewLookup(tt.rules)
			foundRule, err := lookup.Find(tt.searchRuleID)

			if tt.expectedErrorStr != "" {
				require.Equal(t, tt.expectedErrorStr, err.Error())
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.searchRuleID, foundRule.ID)
				if tt.expectedTitle != "" {
					require.Equal(t, tt.expectedTitle, foundRule.Title)
				}
			}
		})
	}
}
