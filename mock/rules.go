package mock

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
)

// MakeRules creates a set of mock rules for testing purposes.
// It accepts optional functions to modify the rules.
func MakeRules(rs []rules.Rule) rules.Rules {
	baseRules := []rules.Rule{
		MakeRule("mock_rule_1"),
		MakeRule("mock_rule_2"),
	}
	baseRules = append(baseRules, rs...)
	rules := MakeEmptyRules(baseRules)

	return rules
}

// MakeEmptyRules creates an empty set of mock rules for testing purposes.
// It accepts optional functions to modify the rules.
func MakeEmptyRules(rs []rules.Rule) rules.Rules {
	rules := rules.Rules{
		Title: "Mock Rules",
		Rules: []rules.Rule{},
	}

	rules.Rules = append(rules.Rules, rs...)

	return rules
}

// MakeRule creates a single mock rule with the given ID.
// It generates a description and remediation based on the ID.
func MakeRule(ruleID string) rules.Rule {
	rule := rules.Rule{
		ID:          ruleID,
		Title:       fmt.Sprintf("Test Rule Title %s", ruleID),
		Description: fmt.Sprintf("This is a mock rule created for testing purposes. The ID is %s.", ruleID),
		Remediation: fmt.Sprintf("As this is a mock rule, no actual remediation is required, The ID is %s.", ruleID),
	}

	return rule
}
