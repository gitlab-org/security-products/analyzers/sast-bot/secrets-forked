# secrets analyzer

secrets analyzer performs Secret Detection scanning. It reports possible secret leaks, like application tokens and cryptographic keys, in the source code and files contained in your project.

The analyzer wraps [Gitleaks](https://github.com/gitleaks/gitleaks) tool, and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast), [IaC](https://docs.gitlab.com/ee/user/application_security/iac_scanning), or [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Rule management

The base set of rules are managed at
https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-rules

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
