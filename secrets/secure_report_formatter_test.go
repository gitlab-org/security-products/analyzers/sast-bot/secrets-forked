package secrets_test

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	tmock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/mock"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/secrets"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description string
		reportPath  string
		wantReport  string
	}{
		{
			description: "small full history",
			reportPath:  "../testdata/reports/gitleaks-full-history-small.json",
			wantReport:  "../testdata/expect/gl-secret-detection-report-full-history-small.json",
		},
		{
			description: "empty report",
			reportPath:  "../testdata/reports/gitleaks-empty.json",
			wantReport:  "../testdata/expect/gl-secret-detection-report-empty.json",
		},
	}

	for _, test := range tests {
		t.Log(test.description)
		f, err := os.Open(test.reportPath)
		require.NoError(t, err)
		reader := bufio.NewReader(f)

		mockFinder := rules.NewMockFinder()
		mockFinder.On("Find", tmock.Anything).Return(nil, nil)

		got, err := secrets.NewSecureReportFormatter(mockFinder).Build(reader, &ruleset.Config{})
		require.NoError(t, err)

		f.Close()

		wantReportBytes, _ := os.ReadFile(test.wantReport)
		gotReportBytes, err := json.MarshalIndent(got, "", "  ")
		require.NoError(t, err)

		require.Equal(t, strings.Trim(string(wantReportBytes), "\n"), strings.Trim(string(gotReportBytes), "\n"))
	}
}

func TestConvertTitleAndDescription(t *testing.T) {

	tests := []struct {
		name        string
		lookupRule  *rules.Rule
		lookupError error
	}{
		{
			name: "When rule is found, name and description come from rules",
			lookupRule: func() *rules.Rule {
				rule := mock.MakeRule("mock_rule_1")
				return &rule
			}(),
			lookupError: nil,
		},
		{
			name:        "When rule is not found, name and description come from gitleaks report",
			lookupRule:  nil,
			lookupError: fmt.Errorf("mock_rule_1 not found"),
		},
	}

	gitleaksReport := []gitleaks.Secret{
		mock.MakeSecret(
			gitleaks.WithRuleID("ruleid_1"),
		),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reportData, err := json.Marshal(gitleaksReport)
			require.NoError(t, err)

			reader := strings.NewReader(string(reportData))

			mockFinder := rules.NewMockFinder()
			mockFinder.On("Find", tmock.Anything).Return(tt.lookupRule, tt.lookupError)

			got, err := secrets.NewSecureReportFormatter(mockFinder).Build(reader, &ruleset.Config{})
			require.NoError(t, err)

			require.Len(t, got.Vulnerabilities, 1)

			if tt.lookupRule == nil {
				require.Equal(t, gitleaksReport[0].Description, got.Vulnerabilities[0].Name)
				require.Equal(t, fmt.Sprintf("%s secret has been found in commit %s.", gitleaksReport[0].Description, gitleaksReport[0].Commit), got.Vulnerabilities[0].Description)
			} else {
				require.Equal(t, tt.lookupRule.Title, got.Vulnerabilities[0].Name)
				require.Equal(t, fmt.Sprintf("%s\n\n%s", tt.lookupRule.Description, tt.lookupRule.Remediation), got.Vulnerabilities[0].Description)
			}
		})
	}
}

func TestConvertNoGit(t *testing.T) {
	cwd, err := os.Getwd()
	require.NoError(t, err)

	gitleaksReport := []gitleaks.Secret{
		mock.MakeSecret(
			gitleaks.WithRuleID("ruleid_1"),
			gitleaks.WithFile(filepath.Join(cwd, "secrets.txt")),
			gitleaks.WithCommit(""),
		),
		mock.MakeSecret(
			gitleaks.WithRuleID("ruleid_2"),
			gitleaks.WithFile(filepath.Join(cwd, "secrets_a.txt")),
			gitleaks.WithCommit(""),
		),
	}

	reportData, err := json.Marshal(gitleaksReport)
	require.NoError(t, err)

	reader := strings.NewReader(string(reportData))

	mockFinder := rules.NewMockFinder()
	mockFinder.On("Find", tmock.Anything).Return(nil, nil)
	got, err := secrets.NewSecureReportFormatter(mockFinder).Build(reader, &ruleset.Config{})
	require.NoError(t, err)

	require.Len(t, got.Vulnerabilities, 2)
	require.Equal(t, "secrets.txt", got.Vulnerabilities[0].Location.File)
	require.Equal(t, "secrets_a.txt", got.Vulnerabilities[1].Location.File)
}
