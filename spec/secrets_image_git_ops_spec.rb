require 'tmpdir'
require 'English'

require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'

# These are tests that run against clones of a sample git repository.  This is
# meant to simlate checkouts in a GitLab pipeline.
#
# We have tests for both "small" (fewer than 20 commits) and
# "large" (more than 50 commits) branches.  We should test both sizes,
# because the initial chechkout for a large branch may not contain all the
# commits.  (The default GIT_DEPTH is 50 for older GitLab projects,
# and 20 for newer projects.  These tests uses a git depth of 50.)
#
# Branches in sample git repository "source_git_repo":
#
#   main: one commit with unredacted secret
#   small_branch_no_secret: 10 commits
#   large_branch_no_secret: 70 commits
#   # large_branch_one_secret has one secret not retrieved by initial GIT_DEPTH
#   large_branch_one_secret:
#     commit #1: commit with no secret
#     commit #2: commit with unredacted secret
#     commit #3: commit redacting secret in commit #2
#     commits #4-#70: additional commits with no secrets
#   # large_branch_two_secrets has two secrets, only one of which is retrieved
#   # with initial GIT_DEPTH
#   large_branch_two_secrets:
#     commit #1: commit with no secret in new_file_1.txt
#     commit #2: commit with unredacted secret in new_file_1.txt
#     commit #3: commit redacting secret in new_file_1.txt
#     commits #4-#68: additional commits with no secrets
#     commit #69: commit with unredacted secret in new_file_2.txt
#     commit #70: commit redacting secret in new_file_2.txt


RSpec.shared_examples "correct commit count" do |number|
  let(:number) { number }
  it 'scans the correct number of commits' do
    expect(scan.combined_output).to match(/#{number} commits scanned./)
  end
end

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'secrets:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', false)
  end

  def target_mount_dir
    '/app'
  end

  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = "gl-secret-detection-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:global_vars) { {} }

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }
    let(:mount_points) { {} }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-secret-detection-report.json',
        privileged: privileged,
        mount_points: mount_points)
    end

    let(:report) { scan.report }

    context 'with git clone' do
      # create fresh git clone for every context
      before(:context) do
        @temp_dir = Dir.mktmpdir
      end
      
      after(:context) do
        FileUtils.remove_entry @temp_dir
      end

      let(:git_depth) { 50 } # simulate GIT_DEPTH of a GitLab job
      let(:branch) { 'any' }
      let(:script) do
        <<~HERE
        #!/bin/sh
    
        set -ex
    
        BRANCH="#{branch}"
    
        git init
        git remote add origin file:///source_git_repo
        git fetch --depth #{git_depth} origin
        git checkout ${BRANCH}
    
        # Run the analyzer
        /analyzer run
        HERE
      end

      let(:mount_points) do
        {
          File.join(fixtures_dir, 'source_git_repo') => '/source_git_repo'
        }
      end

      let(:target_dir) { @temp_dir }

      let(:global_vars) do
        {
          'CI_DEFAULT_BRANCH': 'main',
          'CI_COMMIT_BRANCH': branch
        }
      end

      # SHAs from source_git_repo repository
      let(:git_repo_shas) do
        {
          DEFAULT_COMMIT_BEFORE_SHA: '0000000000000000000000000000000000000000',
          MAIN_BRANCH_HEAD_SHA: '63b99553517a3e48a3f1c45ef9a587f48b0bace8',
          SMALL_BRANCH_NO_SECRET_HEAD_SHA: 'cce08cdb294b0e795acf79ea4dc4e8e3ab4f5d28',
          LARGE_BRANCH_NO_SECRET_HEAD_SHA: '0d719e9f2c5d478c8d8744f3b105e60e7bb18461',
          LARGE_BRANCH_NO_SECRET_FIRST_COMMIT_SHA: '7a99511656c356f49eb6eafa80c7e68a717bb898',
          LARGE_BRANCH_ONE_SECRET_HEAD_SHA: 'af0506f160e409773ac643d221e0b2074fd3b74e',
          LARGE_BRANCH_TWO_SECRETS_HEAD_SHA: '81135b29a9ad96423293d80264e8a098a03df5cb'
        }
      end

      context 'with push event' do
        # MAIN_BRANCH_HEAD_SHA is good for a full branch.  However a real push event won't
        # use the branch point, because CI_COMMIT_BEFORE_SHA is always the
        # default `0000000` SHA for the first push of a git branch to GitLab.
        let(:commit_before_sha) { git_repo_shas[:MAIN_BRANCH_HEAD_SHA] }
        let(:variables) do
          {
            'CI_COMMIT_BEFORE_SHA': commit_before_sha,
            'CI_COMMIT_SHA': commit_sha
          }
        end

        context 'with small_branch_no_secret checked out' do
  
          let(:branch) { 'small_branch_no_secret' }
          let(:commit_sha) { git_repo_shas[:SMALL_BRANCH_NO_SECRET_HEAD_SHA] }
  
          it_behaves_like 'successful scan'
  
          it_behaves_like 'correct commit count', 10
  
          describe 'created report' do
            it_behaves_like 'empty report'
          end
        end
  
        context 'with large_branch_no_secret checked out' do
  
          let(:branch) { 'large_branch_no_secret' }
          let(:commit_sha) { git_repo_shas[:LARGE_BRANCH_NO_SECRET_HEAD_SHA] }

          describe 'scanning all commits' do
            it_behaves_like 'correct commit count', 70
    
            it_behaves_like 'successful scan'
    
            describe 'created report' do
              it_behaves_like 'empty report'
            end
          end

          # scan a smaller range of commits, rather than every commit in the branch
          # this is the behavior for subsequent pushes in a branch pipeline.
          # see "Push event" in
          # https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/index.html#coverage
          describe 'scanning commit range' do
            let(:commit_before_sha) { git_repo_shas[:LARGE_BRANCH_NO_SECRET_FIRST_COMMIT_SHA] }

            it_behaves_like 'correct commit count', 69
    
            it_behaves_like 'successful scan'
    
            describe 'created report' do
              it_behaves_like 'empty report'
            end
          end

          describe 'scanning invalid range after rewritten git history' do
            # Some of the tests in this group are marked as pending.  These tests
            # assert the desired behavior, but it's not implemented yet.
            # See https://gitlab.com/gitlab-org/gitlab/-/issues/434895.

            # simulate rewritten history with an invalid SHA
            let(:commit_before_sha) { 'ffffffaaaaaa0000000000000000000000000000' }

            # see https://gitlab.com/gitlab-org/gitlab/-/issues/434895
            pending('not --no-git scan') do
              it_behaves_like 'correct commit count', 70
            end
    
            it_behaves_like 'successful scan'
    
            describe 'created report' do
              # see https://gitlab.com/gitlab-org/gitlab/-/issues/434895
              pending ('does not scan unrelated files') do
                it_behaves_like 'empty report'
              end
            end
          end
        end

        context 'with large_branch_one_secret checked out' do
          let(:branch) { 'large_branch_one_secret' }
          let(:commit_sha) { git_repo_shas[:LARGE_BRANCH_ONE_SECRET_HEAD_SHA] }

          describe 'scanning all commits' do

            it_behaves_like 'correct commit count', 70

            it_behaves_like 'successful scan'

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) do
                  parse_expected_report('large_branch_one_secret_full_scan')
                end
              end  
              it_behaves_like 'valid report'
            end
          end

          describe 'scanning first push' do
            # first push will always be the default sha
            let(:commit_before_sha) { git_repo_shas[:DEFAULT_COMMIT_BEFORE_SHA] }

            # The first push will only scan the most recent commit
            it_behaves_like 'correct commit count', 1

            it_behaves_like 'successful scan'

            describe 'created report' do
              # The first push will not detect secrets beyond the most recent
              # commit on the branch.  This is by design.
              it_behaves_like 'empty report'
              it_behaves_like 'valid report'
            end
          end
        end

        context 'with large_branch_two_secrets checked out' do
          let(:branch) { 'large_branch_two_secrets' }
          let(:commit_sha) { git_repo_shas[:LARGE_BRANCH_TWO_SECRETS_HEAD_SHA] }

          describe 'scanning all commits' do

            it_behaves_like 'correct commit count', 70

            it_behaves_like 'successful scan'

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) do
                  parse_expected_report('large_branch_two_secrets_full_scan')
                end
              end  
              it_behaves_like 'valid report'
            end
          end

          describe 'scanning first push' do
            # first push will always be the default sha
            let(:commit_before_sha) { git_repo_shas[:DEFAULT_COMMIT_BEFORE_SHA] }

            # The first push will only scan the most recent commit
            it_behaves_like 'correct commit count', 1

            it_behaves_like 'successful scan'

            describe 'created report' do
              # The first push will not detect secrets beyond the most recent
              # commit on the branch.  This is by design.
              it_behaves_like 'empty report'
              it_behaves_like 'valid report'
            end
          end
        end
      end

      context 'with merge request event' do
        let(:mr_diff_base_sha) { git_repo_shas[:MAIN_BRANCH_HEAD_SHA] }
        let(:commit_sha) {  }
        let(:variables) do
          {
            'CI_MERGE_REQUEST_DIFF_BASE_SHA': mr_diff_base_sha,
            'CI_COMMIT_SHA': commit_sha
          }
        end

        context 'with small_branch_no_secret checked out' do
  
          let(:branch) { 'small_branch_no_secret' }
          let(:commit_sha) { git_repo_shas[:SMALL_BRANCH_NO_SECRET_HEAD_SHA] }
  
          it_behaves_like 'successful scan'
  
          it_behaves_like 'correct commit count', 10
  
          describe 'created report' do
            it_behaves_like 'empty report'
          end
        end
  
        context 'with large_branch_no_secret checked out' do
  
          let(:branch) { 'large_branch_no_secret' }
          let(:commit_sha) { git_repo_shas[:LARGE_BRANCH_NO_SECRET_HEAD_SHA] }
  
          it_behaves_like 'correct commit count', 70
  
          it_behaves_like 'successful scan'
  
          describe 'created report' do
            it_behaves_like 'empty report'
          end
        end

        context 'with large_branch_one_secret checked out' do

          let(:branch) { 'large_branch_one_secret' }
          let(:commit_sha) { git_repo_shas[:LARGE_BRANCH_ONE_SECRET_HEAD_SHA] }

          it_behaves_like 'correct commit count', 70

          it_behaves_like 'successful scan'

          describe 'created report' do
            it_behaves_like 'non-empty report'
            it_behaves_like 'recorded report' do
              let(:recorded_report) do
                parse_expected_report('large_branch_one_secret_merge_request')
              end
            end  
            it_behaves_like 'valid report'
          end
        end
      end
    end
  end

end
