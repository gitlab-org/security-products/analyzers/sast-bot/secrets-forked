package main

import (
	"io"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/config"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/secrets"
)

func convert(reader io.Reader, _ string, rulesetConfig *ruleset.Config) (*report.Report, error) {
	rulesPath, err := config.Path(rulesetConfig)
	if err != nil {
		return nil, err
	}

	parser := rules.NewParser()
	parsedRules, err := parser.Parse(rulesPath)
	if err != nil {
		return nil, err
	}

	lookup := rules.NewLookup(*parsedRules)
	return secrets.NewSecureReportFormatter(&lookup).Build(reader, rulesetConfig)
}
