package gitleaks_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/mock"
)

func TestSecret_Identifiers(t *testing.T) {
	tests := []struct {
		name     string
		secret   gitleaks.Secret
		expected []report.Identifier
	}{
		{
			name: "With Rule ID",
			secret: mock.MakeSecret(
				gitleaks.WithRuleID("rule_id_1"),
			),
			expected: []report.Identifier{
				{
					Type:  report.IdentifierType("gitleaks_rule_id"),
					Name:  "Gitleaks rule ID rule_id_1",
					Value: "rule_id_1",
				},
			},
		},
		{
			name: "Without Rule ID",
			secret: mock.MakeSecret(
				gitleaks.WithRuleID(""),
			),
			expected: []report.Identifier{
				{
					Type:  report.IdentifierType("gitleaks_rule_id"),
					Name:  "Gitleaks rule ID description",
					Value: "description",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.expected, tt.secret.Identifiers())
		})
	}
}

func TestSecret_Location(t *testing.T) {
	tests := []struct {
		name     string
		secret   gitleaks.Secret
		expected report.Location
	}{
		{
			name: "With Commit",
			secret: gitleaks.Secret{
				File:      "test.go",
				StartLine: 10,
				Commit:    "abcdef1234567890",
				Author:    "John Doe",
				Date:      "2023-06-01",
				Message:   "Test commit",
			},
			expected: report.Location{
				File:      "test.go",
				LineStart: 10,
				LineEnd:   10,
				Commit: &report.Commit{
					Author:  "John Doe",
					Date:    "2023-06-01",
					Message: "Test commit",
					Sha:     "abcdef1234567890",
				},
			},
		},
		{
			name: "Without Commit",
			secret: gitleaks.Secret{
				Commit:    "",
				File:      "test.go",
				StartLine: 10,
			},
			expected: report.Location{
				File:      "test.go",
				LineStart: 10,
				LineEnd:   10,
				Commit: &report.Commit{
					Sha: "0000000", // defaultSha value
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.expected, tt.secret.Location())
		})
	}
}

func TestSecret_CompareKey(t *testing.T) {
	tests := []struct {
		name     string
		secret   gitleaks.Secret
		expected string
	}{
		{
			name: "With Rule and Fingerprint",
			secret: gitleaks.Secret{
				File:   "test.go",
				Rule:   "test-rule",
				Secret: "test-secret",
			},
			expected: "test.go:9caf06bb4436cdbfa20af9121a626bc1093c4f54b31c0fa937957856135345b6:test-rule",
		},
		{
			name: "Without Rule, With Description and Fingerprint",
			secret: gitleaks.Secret{
				File:        "test.go",
				Description: "test-description",
				Secret:      "test-secret",
			},
			expected: "test.go:9caf06bb4436cdbfa20af9121a626bc1093c4f54b31c0fa937957856135345b6:test-description",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.expected, tt.secret.CompareKey())
		})
	}
}

func TestSecret_DescriptionWithCommit(t *testing.T) {
	tests := []struct {
		name     string
		secret   gitleaks.Secret
		expected string
	}{
		{
			name: "With Commit",
			secret: gitleaks.Secret{
				Description: "Test secret",
				Commit:      "abcdef1234567890",
			},
			expected: "Test secret secret has been found in commit abcdef12.",
		},
		{
			name: "Without Commit",
			secret: gitleaks.Secret{
				Description: "Test secret",
			},
			expected: "Test secret",
		},
		{
			name: "With Short Commit",
			secret: gitleaks.Secret{
				Description: "Test secret",
				Commit:      "abcd",
			},
			expected: "Test secret secret has been found in commit abcd.",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.expected, tt.secret.DescriptionWithCommit())
		})
	}
}
